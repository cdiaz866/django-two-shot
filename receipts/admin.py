from django.contrib import admin
from .models import Receipt, ExpenseCategory, Account
# Register your models here.

class AdminReceipt(admin.ModelAdmin):
    list_display = ('name','vendor', 'total', 'tax', 'date', 'purchaser', 'category','account')
admin.site.register(Receipt, AdminReceipt)

class AdminExpenseCategory(admin.ModelAdmin):
    list_display = ('name','owner')
admin.site.register(ExpenseCategory, AdminExpenseCategory)

class AdminAccount(admin.ModelAdmin):
    list_display = ('name', 'number', 'owner')
admin.site.register(Account,AdminAccount)
